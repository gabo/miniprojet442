
#ifndef dejadef
#define dejadef

// definition des pièces du jeux, 3 bits dans l'échéquier permet de stocqué la totalité de l'images
#define caselibre    0
#define pion     1
#define cavalier 2
#define fous     3
#define reine    4
#define rois     5
#define maskPiece 0b111 // masque des pieces
// couleurs
#define noire    1<<4
#define blanc    0<<4
#define maskCouleur noire
#define Lenligne 480
#define Lencolonne 272

//#include "jeuechec.c"
void ecran(unsigned char * bufferLCD); // compositeur de l'ecran

void resetJeux(void); //

// fonction
//void ecran(unsigned char *);

#endif
